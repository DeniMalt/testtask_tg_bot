import logging
import sys
import time
from aiogram import Bot, types, Dispatcher, executor


from utils import translator
from db import create_table
from db import insert_log


TOKEN = "6574599854:AAF_ciO6nokqW-1-acVYXAiFgaTDi3uAbxM"
bot = Bot(token=TOKEN)
dp = Dispatcher(bot=bot)


@dp.message_handler(commands=['start'])
async def start_handler(message: types.Message):
    user_id = message.from_user.id
    user_name = message.from_user.first_name
    user_full_name = message.from_user.full_name
    logging.info(f'{user_id=} {user_full_name=} {time.asctime()}')
    await message.reply(f"Привет, {user_name},"
    f" я бот-переводчик, умею переводить текст с английского на русский")


@dp.message_handler(commands=['translate'])
async def translate(message: types.Message):
    await message.answer("Введите текст на английском языке, который вы бы хотели перевести")


@dp.message_handler()
async def get_text_from_message(message: types.Message):
    translated_text = await translator(message.text)
    user_id = message.from_user.id
    user_name = message.from_user.first_name
    user_full_name = message.from_user.full_name
    await insert_log(user_id, user_name, user_full_name, message.text, translated_text)
    await message.answer(f'Перевод: {translated_text}')


if __name__ == '__main__':
    time.sleep(3)
    create_table()
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    executor.start_polling(dp)
