import psycopg2


async def conn():
    connect = psycopg2.connect(dbname="db", password="hackme", host="db", user="user")
    connect.autocommit = True
    return connect
