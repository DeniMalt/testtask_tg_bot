from .connection import conn


async def insert_log(user_id: int,
                     user_name: str, user_full_name: str,
                     text_of_message: str,
                     translated_text: str) -> None:
    connect = await conn()
    with connect.cursor() as cursor:
        cursor.execute(f"""insert into logs values
        ('{user_id}', '{user_name}', '{user_full_name}', '{text_of_message}', '{translated_text}')""")
