from asgiref.sync import async_to_sync
from .connection import conn


@async_to_sync
async def create_table() -> None:
    connect = await conn()
    try:
        with connect.cursor() as cursor:
            cursor.execute("""
            create table logs(
            user_id integer not null,
            user_name varchar(1000) not null,
            user_full_name varchar(1000) not null,
            text_for_translate varchar(1000000) not null,
            translated_text varchar(1000000) not null
            )""")
    except:
        pass
