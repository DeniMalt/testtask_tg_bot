import deepl


async def translator(text: str) -> str:
    return deepl.translate(source_language='en', target_language='ru', text=text)
