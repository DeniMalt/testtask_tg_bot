Запуск бота:
    docker-compose up -d  

Структура проекта:  
app - основной код приложения,    
utils - перевод с английского на русский,    
db - работа с бд,    
.env - файл с переменными окружения.  

Для перевода текста через deepl используется библиотека deepl-translate (она работает посредством отправки запросов на api deepl, поэтому есть ограничение на количество запросов, поэтому если бот через несколько запросов перестанет переводить текст - это нормально, нужно просто немного подождать, возможности использовать api deepl или их офиц. библиотеку не было, так как нужен ключ аутентификации, а для его получения - инностранная банковская карта).    

История переведенных сообщений хранится в бд, поэтому чтобы посмотреть переведенные сообщения какого - то из пользователей можете просто зайти в pgadmin и посмотреть историю нужного пользователя.  

Ник бота: @Raul_Translator_Bot  