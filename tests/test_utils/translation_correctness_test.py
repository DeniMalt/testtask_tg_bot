import pytest

from app.utils.translator import translator


@pytest.mark.asyncio
async def test_delete_user():
    str_lst = ['hellow', 'world',
    'One of the charms of mathematics is that simple rules can generate complex and fascinating patterns,'
    ' which raise questions whose answers require profound thought']
    answers = ['привет', 'мир', 'Одна из прелестей математики заключается в том,'
    ' что простые правила могут порождать сложные и увлекательные закономерности, которые ставят вопросы, ответы на котор'
    'ые требуют глубокого осмысления']
    for i in range(len(str_lst)):
        res = await translator(str_lst[i])
        assert res == answers[i]
